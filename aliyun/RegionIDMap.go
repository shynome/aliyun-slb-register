package aliyun

// RegionIDMap aliyun region id maps
var RegionIDMap = map[string]string{
	"华北1": "cn-qingdao",     //青岛 - 2
	"华北2": "cn-beijing",     //北京 - 7
	"华北3": "cn-zhangjiakou", //张家口 - 2
	"华北5": "cn-huhehaote",   //呼和浩特 - 1
	"华东1": "cn-hangzhou",    //杭州 - 7
	"华东2": "cn-shanghai",    //上海 - 5
	"华南1": "cn-shenzhen",    //深圳 - 4
}
