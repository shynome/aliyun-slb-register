package slb

import (
	"github.com/aliyun/alibaba-cloud-sdk-go/sdk"
	"github.com/aliyun/alibaba-cloud-sdk-go/sdk/requests"
	"gitlab.com/shynome/aliyun-slb-register/aliyun"
)

// Config slb
type Config struct {
	AccessKeyID     string
	AccessKeySecret string
	Region          string // 负载均衡所在区域
	Name            string // 负载均衡的自定义名称
	VGroup          string // 负载均衡的中的虚拟服务器组名
	VGroupID        string // 负载均衡的中的虚拟服务器组id
}

// SLB SLB
type SLB struct {
	config *Config
	client *sdk.Client
}

// New slb
func New(config *Config) (slb *SLB, err error) {
	slb = &SLB{}
	slb.config = config
	if slb.client, err = sdk.NewClientWithAccessKey(aliyun.RegionIDMap["xxx"], config.AccessKeyID, config.AccessKeySecret); err != nil {
		return
	}
	return
}

func (slb *SLB) getCommonRequest() (request *requests.CommonRequest) {
	request = requests.NewCommonRequest()
	request.Method = "POST"
	request.Scheme = "https" // https | http
	request.Domain = "slb.aliyuncs.com"
	request.Version = "2014-05-15"
	return
}
