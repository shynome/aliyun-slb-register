module gitlab.com/shynome/aliyun-slb-register

require (
	github.com/aliyun/alibaba-cloud-sdk-go v0.0.0-20190222082822-d7df553f15d2
	github.com/jmespath/go-jmespath v0.0.0-20180206201540-c2b33e8439af // indirect
	github.com/json-iterator/go v1.1.5 // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.1 // indirect
	github.com/satori/go.uuid v1.2.0 // indirect
	github.com/stretchr/testify v1.3.0 // indirect
)
